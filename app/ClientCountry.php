<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCountry extends Model
{
    protected $table = 'ClientCountry';
    protected $fillable = ['idClientCountry','name'];
}
