<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker;

use App;
use App\Http\Requests;

class creativeDashController extends Controller
{
    public function show($dashboard = []){
        $creativeDash = App\CreativeDash::get();
        
        foreach ($creativeDash as $key=>$value) {
        $country = App\ClientCountry::where('idClientCountry',$value->countryId)->first();
            array_push($dashboard,[
                'dashid'=> $value->idDashboard,
                'countryid'=> $value->countryId,
              'country' => $country->name,
              'name' => $value->name,
              'design' => $value->design,
              'adformat' => $value->adformat,
              'handled_by' => $value->handled_by,
            ]) ;
        }
        return view('dataManagement.dashboard')->with([
            'dashboard' => $dashboard,
        ]);
    }
    public function create(Request $request){
        $faker = Faker\Factory::create();
            
        //search exsisting country
        $search = App\ClientCountry::where('name',$request->get('country'));
        if($search->count()==0){
            
            //new country
            $countryID = $faker->randomNumber(5,false);
            $country = new App\ClientCountry;
            $country->idClientCountry = $countryID;
            $country->name = $request->get('country');
            $country->save();
            
            //new creative
            $dash = new App\CreativeDash;
            $dash->idDashboard = $faker->randomNumber(5,false);
            $dash->countryId = $countryID;
            $dash->name = $request->get('nama');
            $dash->design = $request->get('design');
            $dash->adformat = $request->get('adformat');
            $dash->handled_by = $request->get('handledby');
            $dash->save();
        }
        else{
            $result = $search->get()->toArray();
            
            //new creative
            $dash = new App\CreativeDash;
            $dash->idDashboard = $faker->randomNumber(5,false);
            $dash->countryId = $result[0]['idClientCountry'];
            $dash->name = $request->get('nama');
            $dash->design = $request->get('design');
            $dash->adformat = $request->get('adformat');
            $dash->handled_by = $request->get('handledby');
            $dash->save();
        }
        
        return redirect('dashboard');
    }
    public function update(Request $request){
        //new country
        $country = App\ClientCountry::where('idClientCountry',$request->get('countryID'));
        $data = [
            "name" => $request->get('country'),
        ];
        $country->update($data);
        
        //new creative
        $dash = App\CreativeDash::where('idDashboard',$request->get('dashID'));
        $data = [
            "name" => $request->get('nama'),
            "design" => $request->get('design'),
            "adformat" => $request->get('adformat'),
            "handled_by" => $request->get('handledby'),
        ];
        $dash->update($data);
        
        return redirect('dashboard');
    }
    public function delete($id){
        $dash = App\CreativeDash::where('idDashboard',$id)->delete();
        return redirect('dashboard');
    }
}
