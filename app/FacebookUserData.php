<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacebookUserData extends Model
{
    protected $table = 'facebookClient';
    protected $fillable = ['idfacebookUserData','first_name','middle_name','last_name','name','about','devices','idEducation','minimalAge','relation_status','religion','idWork'];
    public $timestamps = false;
}
