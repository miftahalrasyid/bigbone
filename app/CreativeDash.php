<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreativeDash extends Model
{
    protected $table = 'Dashboard';
    protected $fillable = ['idDashboard','countryId','name','design','adformat','handled_by'];
}
