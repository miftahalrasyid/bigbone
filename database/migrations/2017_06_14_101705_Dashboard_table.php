<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DashboardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Dashboard', function (Blueprint $table) {
            $table->increments('idDashboard');
            $table->string('countryId');
            $table->string('name');
            $table->string('design');
            $table->string('adformat');
            $table->string('handled_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Dashboard');
    }
}
