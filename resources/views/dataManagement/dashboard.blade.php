@extends('layouts.app')

@section('content')
        <div id="container">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"> add new record </button>
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
                  <div class="modal-body">
                        <form method="post" action="{{route('addNew')}}">
                            {{ csrf_field() }}
                            <label for="country">Client Country</label>
                            <input type="text" id="country" name="country" class="form-control">
                            <label for="nama">Events Name</label>
                            <input type="text" id="nama" name="nama" class="form-control">
                            <label for="design">Design Category</label>
                            <input type="text" id="design" name="design" class="form-control">
                            <label for="adformat">Adformat</label>
                            <input type="text" id="adformat" name="adformat" class="form-control">
                            <label for="handledby">Handled By</label>
                            <input type="text" id="handledby" name="handledby" class="form-control">
                            <br>
                            <input type="submit" class="btn btn-primary" value="submit">
                        </form> 
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
            <section>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Country</th>
                            <th>Name</th>
                            <th>Design</th>
                            <th>Ad Format</th>
                            <th>Handled By</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dashboard as $key=>$value)
                            <tr>
                                <td>{{$value['country']}}</td>
                                <td>{{$value['name']}}</td>
                                <td>{{$value['design']}}</td>
                                <td>{{$value['adformat']}}</td>
                                <td>{{$value['handled_by']}}</td>
                                <td> <button class="ed open-dialog-delete btn btn-primary btn-xs" data-title="Delete" data-toggle="modal" data-target="#fned{{$key}}"
                                data-id='{{$key}}' data-name-tag='Quiz/Game'>
                            <span class="glyphicon glyphicon-pencil"></span></button></td>
                                <td><button class="del open-dialog-delete btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" 
                                data-id='{{$value["dashid"]}}' data-name-tag='Quiz/Game'>
                            <span class="glyphicon glyphicon-trash"></span></button></td>
                               <input id="countryID{{$key}}" type="hidden" value="{{$value['countryid']}}" name="countryID" />
                            <input id="dashID{{$key}}" type="hidden" value="{{$value['dashid']}}" name="dashID" />
                            
                            </tr>
                        
                        @endforeach

                    </tbody>
                    <!-- Modal -->
                    <div id="fned" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Item</h4>
                  </div>
                  <div class="modal-body">
                        <form method="post" action="{{route('edit')}}">
                            {{ csrf_field() }}
                            <label for="country">Client Country</label>
                            <input type="text" id="countrymdl" name="country" class="form-control" >
                            <label for="nama">Events Name</label>
                            <input type="text" id="namamdl" name="nama" class="form-control" >
                            <label for="design">Design Category</label>
                            <input type="text" id="designmdl" name="design" class="form-control" >
                            <label for="adformat">Adformat</label>
                            <input type="text" id="adformatmdl" name="adformat" class="form-control" >
                            <label for="handledby">Handled By</label>
                            <input type="text" id="handledbymdl" name="handledby" class="form-control" >
                            <input id="countryIDmdl" type="hidden" name="countryID" />
                            <input id="dashIDmdl" type="hidden" name="dashID" />
                            <br>
                            <input type="submit" class="btn btn-primary" value="submit">
                        </form> 
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
                </table>
            </section>
        </div>
@endsection

@section('js')
    <script src="{{asset('js/custom.js')}}"></script>
@endsection