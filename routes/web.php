<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/dashboard','creativeDashController@show');
Route::post('/dashboard/addNew','creativeDashController@create')->name('addNew');
Route::post('/dashboard/edit','creativeDashController@update')->name('edit');
Route::get('/dashboard/delete/{id}','creativeDashController@delete')->name('delete');

Route::get('/api/v1/products/{id}',function($id = null){

    $products = App\Product::find($id,array('id','category','name','quantity','price'));
    
    return Response::json(array(
        'error' => false,
        'products' => $products,
        'status_code' => 200
    ));
});

Route::get('/api/v1/products/',function(){
    
    $products = App\Product::all(array('id','category','name','quantity','price'));
    
    return Response::json(array(
        'error' => false,
        'products' => $products,
        'status_code' => 200
    ));
});

Route::get('/facebook', 'facebookConnectController@show');
Route::get('/facebook/callback/', 'facebookConnectController@callback');

Route::get('/telesign/phoneid/','telesignController@show');

Route::get('/campaign','campaignController@show');